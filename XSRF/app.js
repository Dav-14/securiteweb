const uuidv4 = require('uuid');
const express = require('express');
const bodyParser = require('body-parser')
const session = require('express-session');

const app = express();
const router = express.Router();
const urlencodedParser = bodyParser.urlencoded({ extended: false })


router.use(session({
    cookie: {
        httpOnly: true,
        sameSite: true,
    },
    secret: 'ChangeMe',
    resave: true,
    saveUninitialized: true
}))

router.use(urlencodedParser);

router.get('/',function(req,res){
    //res.sendFile(path.join(__dirname+'/index.html'));
    const token = uuidv4.v4();

    req.session.token = token;

    res.send(`
        <!DOCTYPE html>
        <html lang="fr">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>
        <body>
            <form action="/" method="post">
                <input type="hidden" name="token" value="${token}" id="token">
                <input type="password" name="password" id="password"><br />
                <input type="email" name="email" id="email">
                <button type="submit">VALIDER</button>
            </form>
        </body>
        </html>       
        `
    )
    //__dirname : It will resolve to your project folder.
});


router.post('/', function(req, res){
    const {token, password, email} = req.body;
    //console.log({token, password, email});
    
    if (req.session.token && token && req.session.token == token){
        res.status(201)
        res.send("OOPS")
    }else{
        res.status(418);
        res.send("JE SUIS DINGO");
    }
});
  
app.use('/', router);
  
module.exports = app;