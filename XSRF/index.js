const http = require('http');
const app = require('./app');
const port = process.env.APP_PORT || 3000;


app.set('port', port);
const server = http.createServer(app);

console.log(`Server is running on ${port}`)

server.listen(port);